//
//  DreamJournalCollectionViewController.swift
//  FoodTracker
//
//  Created by  on 10/11/16.
//  Copyright Â© 2016 Apple Inc. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class DreamJournalCollectionViewController: UICollectionViewController {
    
    var entries = [DreamEntry]()

    override func viewDidLoad() {
        super.viewDidLoad()

        //load data
        loadEnteries()
        // Register cell classes
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
    func loadEnteries() {
        let entry1 = DreamEntry(title: "Test Title", content: "Test Content", tag: "Test Tag")
        entries = [entry1!]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


     func tableView (tableView: UITableView, numberOfRowsInSection: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return entries.count
    }

    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath : NSIndexPath)-> UITableViewCell {
    let cellIdentifier = "DreamJournalViewCell"
    // Configure the cell
    let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! DreamJournalTableViewCell
    let entry = entries[indexPath.row]
    
    cell.nameLabel.text = entry.title
    cell.tagLabel.text = entry.tag
    return cell
    }


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

