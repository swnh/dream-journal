//
//  DetailViewController.swift
//  Dream
//
//  Created by  on 9/16/16.
//  Copyright � 2016 Dream Team. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var dreamTitle: UITextField!
    @IBOutlet weak var dreamContent: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var boldButton: UIButton!
    @IBOutlet weak var italicButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryField: UITextField!
    
    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if objects.count == 0 {
            return
        }
            if let label = self.dreamContent {
                label.text = objects[currentIndex]
                if label.text == BLANK_NOTE {
                    label.text = ""
                }
            }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.font = UIFont(name:"TimesNewRomanPS-BoldMT", size:16)
        contentLabel.font = UIFont(name:"TimesNewRomanPS-BoldMT", size:16)
        categoryLabel.font = UIFont(name:"TimesNewRomanPS-BoldMT", size:16)
        boldButton.addTarget(self, action: "bold:", forControlEvents: .TouchUpInside)
        italicButton.addTarget(self, action: "italic:", forControlEvents: .TouchUpInside)
        if (categories.count > 0) {
            categoryField.text = categories[dreamdex]
        }
        detailViewController = self
        dreamContent.becomeFirstResponder()
        self.configureView()
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        if objects.count == 0 {
            return
        }
        objects[currentIndex] = dreamContent.text
        if dreamContent.text == "" {
            objects[currentIndex] = BLANK_NOTE
        }
        categories[dreamdex] = categoryField.text
        saveAndUpdate()
    }

    func saveAndUpdate(){
        masterView?.save()
        masterView?.tableView.reloadData()
    }
    
    func bold(sender: UIButton){
        if(dreamContent.font == UIFont(name:"TimesNewRomanPS-BoldMT", size:16)){
            dreamContent.font = UIFont(name:"TimesNewRomanPSMT", size:16)
        }
        else if(dreamContent.font == UIFont(name:"TimesNewRomanPS-BoldItalicMT", size:16)){
            dreamContent.font = UIFont(name:"TimesNewRomanPS-ItalicMT", size:16)
        }
        else if(dreamContent.font == UIFont(name:"TimesNewRomanPS-ItalicMT", size:16)){
            dreamContent.font = UIFont(name:"TimesNewRomanPS-BoldItalicMT", size:16)
        }
        else{
            dreamContent.font = UIFont(name:"TimesNewRomanPS-BoldMT", size:16)
        }
    }
    
    func italic(sender: UIButton){
        if(dreamContent.font == UIFont(name:"TimesNewRomanPS-ItalicMT", size:16)){
            dreamContent.font = UIFont(name:"TimesNewRomanPSMT", size:16)
        }
        else if(dreamContent.font == UIFont(name:"TimesNewRomanPS-BoldItalicMT", size:16)){
            dreamContent.font = UIFont(name:"TimesNewRomanPS-BoldMT", size:16)
        }
        else if(dreamContent.font == UIFont(name:"TimesNewRomanPS-BoldMT", size:16)){
            dreamContent.font = UIFont(name:"TimesNewRomanPS-BoldItalicMT", size:16)
        }
        else{
            dreamContent.font = UIFont(name:"TimesNewRomanPS-ItalicMT", size:16)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}