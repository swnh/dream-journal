//
//  DreamEntry.swift
//  FoodTracker
//
//  Created by  on 10/11/16.
//  Copyright Â© 2016 Apple Inc. All rights reserved.
//

import UIKit

class DreamEntry {
        //MARK: Properties
    
    var title: String
    var content: String
    var tag: String
    
    init?(title: String, content: String, tag: String)
    {
        self.title = title
        self.content = content
        self.tag = tag
        
        if title.isEmpty || content.isEmpty || tag.isEmpty {
            return nil
        }
    }
}
